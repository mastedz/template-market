module.exports = function(id, callback) {
    var url = "http://www.theimdbapi.org/api/person";
    $.ajax({
        url: url,
        type: 'GET',
        dataType: 'json',
        data: {
            person_id: id
        }
    })
        .done(function(e) {
            if(typeof callback == "function") {
                callback(e);
            } else {
                return e;
            }
        })
        .fail(function(e) {
            console.log(e);
        });
}