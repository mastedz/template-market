import { setTimeout } from 'timers';

/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./gentelella.js');
require('./ajax-form.js');
require('./jquery.canvasjs.min.js');

var AJAX_LOAD = true;

// imdb-fetch



$(document).ready(function() {
  $('.dd').nestable({
    maxDepth: 3
  });
  $('#save_menu_order').on('click', function(event) {
    event.preventDefault();
    $("#delete-menu-form > input").val(JSON.stringify($('.dd').nestable('serialize')));
    $("#delete-menu-form").submit();
  });

    $('.tags').each(function(i, el) {
        el = $(el);
        el.tokenfield({
        autocomplete: {
            source: function( request, response ) {
                AJAX_LOAD = false;
                $.ajax( {
                    url: el.attr('data-url'),
                    dataType: "json",
                    data: {
                        term: request.term
                    },
                    success: function( data ) {
                        response( data );
                        AJAX_LOAD = true;
                    }
                } );
            }
        },
        showAutocompleteOnFocus: true
    });
    });

   // $('#tags_1').importTags('foo,bar,baz');
  // tinymce
  tinymce.init({
    selector: '.content',
    height: 300,
    theme: 'modern',
    plugins: [
      'preview code insertdatetime media nonbreaking save table contextmenu directionality',
      'template paste textcolor colorpicker textpattern imagetools codesample toc help emoticons hr',
      'image',
    ],
    toolbar1: 'newdocument | print searchreplace | undo redo | image | insert | bullist numlist outdent indent  fontselect | fontsizeselect | bold italic underline  | alignleft aligncenter alignright alignjustify | removeformat | preview code visualblocks advcode fullscreen help |',
    image_advtab: true,
    content_css: [
      '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
      '//www.tinymce.com/css/codepen.min.css'
    ]
  });
  $(".parent").click(function() {
    var parent_id = $(this).attr('child');
    $( "." + parent_id ).toggle();
  });
  $('.ajax_form').ajaxForm();

    $('#formActor').on('submit',function (event) {
        event.preventDefault();
        tinyMCE.triggerSave();
    });
    $('#formActor').ajaxForm({
        success: function() {
            setTimeout(function () {
                window.location = CREATE_ACTOR_SUCCESS_LINK;
            }, 1000);
        }
    });

  $('#create_menu_form').ajaxForm({
    beforeSubmit: function() {
      tinyMCE.triggerSave();
    },
    success: function() {
      setTimeout(function () {
        window.location = CREATE_MENU_SUCCESS_LINK;
      }, 1000);
    }
  });
  $('#create_category_form').ajaxForm({
    beforeSubmit: function() {
      tinyMCE.triggerSave();
    },
    success: function() {
      setTimeout(function () {
        window.location = CREATE_CATEGORY_SUCCESS_LINK;
      }, 1000);
    }
  });
  $('#delete-menu-form').ajaxForm({
    success: function(e) {
      setTimeout(function () {
        window.location = DELETE_MENU_SUCCESS_LINK;
      }, 1000);
    }
  });

  //

    $('#formTag').ajaxForm({
        beforeSubmit: function() {
            tinyMCE.triggerSave();
        },
        success: function(e) {
            setTimeout(function () {
                window.location = CREATE_TAG_SUCCESS_LINK;
            }, 1000);
        }
    });

  $(".delete-menu").on('click', function(event) {
    event.preventDefault();
    $('.modal-delete-menu').modal('show');
    $("#delete-menu-modal-button").attr('data-url', $(this).attr('href'));
  });
  $("#delete-menu-modal-button").on('click', function(event) {
    event.preventDefault();
    var url = $(this).attr('data-url');
    $("#delete-menu-form").attr('action', url);
    $("#delete-menu-form").submit();
  });
  $("#menu_position").on('change', function() {
    NProgress.start();
    $.ajax({
      url: PARENT_MENU_JSON_LINK,
      type: 'GET',
      dataType: 'json',
      data: {
        position: $(this).val()
      }
    })
    .done(function(e) {
      if(e.status == "success") {
        var _option_el;
        var selected = $("#parent_menu").attr('data-selected');
        $("#parent_menu").empty().append('<option value=""></option>');
        $.each(e.data, function(idx, data) {
          _option_el = document.createElement("option");
          $(_option_el).val(data.id_menu).text(data.title).addClass('bold').attr('data-menu-id', data.id_menu).appendTo('#parent_menu');
          if(selected == data.id_menu) {
            $(_option_el).attr('selected', 'selected');
          }
          var id_menu = data.id_menu;
          $.ajax({
            url: CHILD_MENU_JSON_LINK,
            type: 'GET',
            dataType: 'json',
            data: {
              id: id_menu
            }
          })
          .done(function(e) {
            if(e.status == "success") {
              $.each(e.data, function(idx, data) {
                _option_el = document.createElement("option");
                $(_option_el).val(data.id_menu).text('-- ' + data.title).insertAfter('option[data-menu-id=' + id_menu + ']');
                if(selected == data.id_menu) {
                  $(_option_el).attr('selected', 'selected');
                }
              });
            }
          });
        });
        NProgress.done();
      }
    })
    .fail(function() {
      NProgress.done();
    });
  });
  $("#menu_position").trigger('change');
  $("#bulk-delete-button").on('click', function(event) {
    event.preventDefault();
    $(".modal-bulk-delete").modal('show');
  });
  $("#bulk-delete-modal-button").on('click', function(event) {
    event.preventDefault();
    $("#bulk-delete-form").submit();
  });
  var
    _default_urlformat_link = $("#create_menu_form").attr('action'),
    _default_urlformat_text = $("#save-btn").html()
  ;
  $(".edit_urlformat").on('click', function(event) {
    event.preventDefault();
    $("#create_menu_form").attr('action', $(this).attr('href'));
    $("#urlformat_text").val($(this).attr('data-text'));
    if($("#urlformat_endpoint").length > 0)
      $("#urlformat_endpoint").val($(this).attr('data-endpoint'));
    $("#save-btn").html('<i class="fa fa-save" aria-hidden="true"></i> Update');
    $("#cancel-btn").show();
    $("#urlformat_method").val('PATCH');
  });
  $("#cancel-btn").on('click', function(event) {
    event.preventDefault();
    $("#create_menu_form").attr('action', _default_urlformat_link);
    $("#urlformat_text").val('');
    if($("#urlformat_endpoint").length > 0)
      $("#urlformat_endpoint").val('');
    $("#save-btn").html(_default_urlformat_text);
    $("#cancel-btn").hide();
    $("#urlformat_method").val('POST');
  });
  var first_fetch_menu_type = false;
  $("select[name=menu_type]").on('change', function(event) {
    event.preventDefault();
    var value = $(this).val();
    if(value == 'external') {
      $('select[name=menu_type]').next('select').remove();
      $('select[name=menu_type]').next('input').remove();
      var input = document.createElement("input");
      $(input).addClass('form-control');
      input.style = "margin-top: 2px";
      $(input).attr('name', 'menu_value');
      $(input).insertAfter('select[name=menu_type]');
      if(!first_fetch_menu_type) {
        first_fetch_menu_type = true;
        $(input).val($("input[name=current_val]").val());
      }
      return;
    }
    $.ajax({
      url: MENU_TYPE_LINK,
      dataType: 'json',
      data: {
        type: value
      }
    })
    .done(function(e) {
      $('select[name=menu_type]').next('select').remove();
      $('select[name=menu_type]').next('input').remove();
      var select = document.createElement("select");
      $(select).addClass('form-control');
      select.style = "margin-top: 2px";
      $.each(e, function(idx, data) {
        if(!first_fetch_menu_type) {
          if(data.id == $("input[name=current_val]").val())
            $(select).append('<option value="'+ data.id +'" selected>'+ data.title +'</option>');
          else
            $(select).append('<option value="'+ data.id +'">'+ data.title +'</option>');
        }
        else
          $(select).append('<option value="'+ data.id +'">'+ data.title +'</option>');
      });
      if(!first_fetch_menu_type)
        first_fetch_menu_type = true;
      $(select).attr('name', 'menu_value');
      $(select).insertAfter('select[name=menu_type]');
    });
  });
  $("select[name=menu_type]").trigger('change');
});

$('.ajax_form').ajaxForm();

$('#submitLogout').click(function (e) {
    e.preventDefault();
    $('#formLogout').submit();
});
//
$('#formSearch').submit(function(e){
    e.preventDefault();
    var input = $('#search').val();
    window.location = $(this).attr('action')+'/'+input;
});


//
$('#delete-form').ajaxForm({
    success: function(e) {
        setTimeout(function () {
            window.location = DELETE_SUCCESS_LINK;
        }, 1000);
    }
});
$('#bulk-delete-form').ajaxForm({
    beforeSubmit: function(e) {
      $('input[name=table_records]').each(function(index, el) {
        $(el).attr('name', 'table_records[]').addClass('table_record_changed');
      });
    },
    success: function(e) {
        $('.table_record_changed').each(function(index, el) {
          $(el).attr('name', 'table_records');
        });
        setTimeout(function () {
            window.location = DELETE_SUCCESS_LINK;
        }, 1000);
    },
    error: function(e) {
      $('.table_record_changed').each(function(index, el) {
        $(el).attr('name', 'table_records');
      });
    }
});
$(".delete").on('click', function(event) {
    event.preventDefault();
    if($(this).attr('id') == 'delete-checked'){
        var checked = "";
        $( ".isdelete" ).each(function() {
            if ($(this).is(":checked"))
            {
                checked += $(this).val()+",";
            }
        });
        $('#delete-form').append("<input type='hidden' name='check' value='"+checked+"'>");
    }
    if($(this).hasClass('option')){
        $('#delete-form').append("<input type='hidden' name='option' value='"+$(this).attr('data-text')+"'>");
    }
    $('.modal-delete').modal('show');
    $("#delete-modal-button").attr('data-url', $(this).attr('href'));

});
$("#delete-modal-button").on('click', function(event) {
    event.preventDefault();
    $("#delete-form").attr('action', $(this).attr('data-url'));
    $("#delete-form").submit();
});


















