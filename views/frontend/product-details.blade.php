@extends('frontend.layout_page')
@section('someCSS')
<link rel="stylesheet" type="text/css" href="{{asset('')}}styles/product_styles.css">
<link rel="stylesheet" type="text/css" href="{{asset('')}}styles/product_responsive.css">
<link rel="stylesheet" type="text/css" href="{{asset('')}}styles/qna.css">
<style type="text/css">
section{
    display: none;
}
</style>
@endsection
@section('content')
    <div class="single_product">
        <div class="container">
            <div class="row">

                <!-- Images -->
                <div class="col-lg-2 order-lg-1 order-2">
                    <ul class="image_list">
                    @if($product_edit->image)
                        @foreach(json_decode($product_edit->image) as $img)
                        <li data-image="{{asset('')}}images/{{$img}}"><img src="{{asset('')}}images/{{$img}}" alt=""></li>
                       
                        
                        @endforeach
                    @else
                    <li data-image="https://www.google.com/url?sa=i&source=images&cd=&cad=rja&uact=8&ved=2ahUKEwifpdnNi4LiAhWLdXAKHcReBJkQjRx6BAgBEAU&url=%2Furl%3Fsa%3Di%26source%3Dimages%26cd%3D%26ved%3D%26url%3Dhttps%253A%252F%252Fwww.brother.ca%252Fen%252Fp%252FSA375%26psig%3DAOvVaw13D_WmoC4L7ZbAc6FsF_Gw%26ust%3D1557066690097191&psig=AOvVaw13D_WmoC4L7ZbAc6FsF_Gw&ust=1557066690097191"><img src="https://www.google.com/url?sa=i&source=images&cd=&cad=rja&uact=8&ved=2ahUKEwifpdnNi4LiAhWLdXAKHcReBJkQjRx6BAgBEAU&url=%2Furl%3Fsa%3Di%26source%3Dimages%26cd%3D%26ved%3D%26url%3Dhttps%253A%252F%252Fwww.brother.ca%252Fen%252Fp%252FSA375%26psig%3DAOvVaw13D_WmoC4L7ZbAc6FsF_Gw%26ust%3D1557066690097191&psig=AOvVaw13D_WmoC4L7ZbAc6FsF_Gw&ust=1557066690097191" alt=""></li>
                    @endif
                    </ul>
                </div>

                <!-- Selected Image -->
                <div class="col-lg-5 order-lg-2 order-1">
                    <div class="image_selected"><img src="{{asset('')}}images/{{$product_edit->image[0]}}" alt=""></div>
                </div>

                <!-- Description -->
                <div class="col-lg-5 order-3">
                    <div class="product_description">

                        <div class="product_category">{{$product_edit->category()->first()->title}}</div>
                        <div class="product_name">{{$product_edit->name}}</div>
                        <div class="rating_r rating_r_4 product_rating"><i></i><i></i><i></i><i></i><i></i></div>
            
                        <div class="order_info d-flex flex-row">
                            <form action="#">
                                <div class="clearfix" style="z-index: 1000;">

                                    <!-- Product Quantity -->
                                    <div class="product_quantity clearfix">
                                        <span>Quantity: </span>
                                        <input id="quantity_input" type="text" pattern="[0-9]*" value="1">
                                        <div class="quantity_buttons">
                                            <div id="quantity_inc_button" class="quantity_inc quantity_control"><i class="fas fa-chevron-up"></i></div>
                                            <div id="quantity_dec_button" class="quantity_dec quantity_control"><i class="fas fa-chevron-down"></i></div>
                                        </div>
                                    </div>

                                    <!-- Product Color -->
                                    <ul class="product_color">
                                        <li>
                                            <span>Color: </span>
                                            <div class="color_mark_container"><div id="selected_color" class="color_mark"></div></div>
                                            <div class="color_dropdown_button"><i class="fas fa-chevron-down"></i></div>

                                            <ul class="color_list">
                                                <li><div class="color_mark" style="background: #999999;"></div></li>
                                                <li><div class="color_mark" style="background: #b19c83;"></div></li>
                                                <li><div class="color_mark" style="background: #000000;"></div></li>
                                            </ul>
                                        </li>
                                    </ul>

                                </div>

                                <div class="product_price">$2000</div>
                                <div class="button_container">
                                    <button type="button" class="button cart_button">Add to Cart</button>
                                    <div class="product_fav"><i class="fas fa-heart"></i></div>
                                </div>
                                
                            </form>
                        </div>
                    </div>

                </div>

            </div>
            <br>
            <main>

  <input id="tab1" type="radio" name="tabs" checked>
  <label for="tab1">Informasi Produk</label>

  <input id="tab2" type="radio" name="tabs">
  <label for="tab2">Review Produk</label>

  <input id="tab3" type="radio" name="tabs">
  <label for="tab3">Diskusi Produk</label>

  
  <section id="content1">
    <p>
     {{$product_edit->description}}
    </p>
   
  </section>

  <section id="content2">
    <p>
      Bacon ipsum dolor sit amet landjaeger sausage brisket, jerky drumstick fatback boudin.
    </p>
    <p>
      Jerky jowl pork chop tongue, kielbasa shank venison. Capicola shank pig ribeye leberkas filet mignon brisket beef kevin tenderloin porchetta. Capicola fatback venison shank kielbasa, drumstick ribeye landjaeger beef kevin tail meatball pastrami prosciutto pancetta. Tail kevin spare ribs ground round ham ham hock brisket shoulder. Corned beef tri-tip leberkas flank sausage ham hock filet mignon beef ribs pancetta turkey.
    </p>
  </section>

  <section id="content3">
   <div style="height: 40%;
    overflow-y: auto;
    overflow: auto;" class="ask"></div>
   <hr>
   @if(\Auth::user())
    <div class="actionBox">
          <form class="form-inline" id="postQuestion" role="form">
            <div class="form-group">
                <input type="hidden" id="uid"  value="{{\Auth::user()->id}}" data-id="{{$product_edit->id}}">
                <textarea class="form-control" id="question" type="text" placeholder="Ajukan Pertanyaan" ></textarea>
            </div>
            <div class="form-group">
                <button style="margin-top: 27px;" id="submit" class="btn btn-primary">Kirim</button>
            </div>
        </form>
    </div>
    @endif
  </section>


  <section id="content4">
    <p>
      Bacon ipsum dolor sit amet landjaeger sausage brisket, jerky drumstick fatback boudin.
    </p>
    <p>
      Jerky jowl pork chop tongue, kielbasa shank venison. Capicola shank pig ribeye leberkas filet mignon brisket beef kevin tenderloin porchetta. Capicola fatback venison shank kielbasa, drumstick ribeye landjaeger beef kevin tail meatball pastrami prosciutto pancetta. Tail kevin spare ribs ground round ham ham hock brisket shoulder. Corned beef tri-tip leberkas flank sausage ham hock filet mignon beef ribs pancetta turkey.
    </p>
  </section>

</main>
        </div>

    </div>

    <!-- Recently Viewed -->

    <div class="viewed">
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="viewed_title_container">
                        <h3 class="viewed_title">Barang Terkait</h3>
                        <div class="viewed_nav_container">
                            <div class="viewed_nav viewed_prev"><i class="fas fa-chevron-left"></i></div>
                            <div class="viewed_nav viewed_next"><i class="fas fa-chevron-right"></i></div>
                        </div>
                    </div>

                    <div class="viewed_slider_container">
                        
                        <!-- Recently Viewed Slider -->

                        <div class="owl-carousel owl-theme viewed_slider">
                            
                            <!-- Recently Viewed Item -->
                            <div class="owl-item">
                                <div class="viewed_item discount d-flex flex-column align-items-center justify-content-center text-center">
                                    <div class="viewed_image"><img src="{{asset('')}}images/view_1.jpg" alt=""></div>
                                    <div class="viewed_content text-center">
                                        <div class="viewed_price">$225<span>$300</span></div>
                                        <div class="viewed_name"><a href="#">Beoplay H7</a></div>
                                    </div>
                                    <ul class="item_marks">
                                        <li class="item_mark item_discount">-25%</li>
                                        <li class="item_mark item_new">new</li>
                                    </ul>
                                </div>
                            </div>

                            <!-- Recently Viewed Item -->
                            <div class="owl-item">
                                <div class="viewed_item d-flex flex-column align-items-center justify-content-center text-center">
                                    <div class="viewed_image"><img src="{{asset('')}}images/view_2.jpg" alt=""></div>
                                    <div class="viewed_content text-center">
                                        <div class="viewed_price">$379</div>
                                        <div class="viewed_name"><a href="#">LUNA Smartphone</a></div>
                                    </div>
                                    <ul class="item_marks">
                                        <li class="item_mark item_discount">-25%</li>
                                        <li class="item_mark item_new">new</li>
                                    </ul>
                                </div>
                            </div>

                            <!-- Recently Viewed Item -->
                            <div class="owl-item">
                                <div class="viewed_item d-flex flex-column align-items-center justify-content-center text-center">
                                    <div class="viewed_image"><img src="{{asset('')}}images/view_3.jpg" alt=""></div>
                                    <div class="viewed_content text-center">
                                        <div class="viewed_price">$225</div>
                                        <div class="viewed_name"><a href="#">Samsung J730F...</a></div>
                                    </div>
                                    <ul class="item_marks">
                                        <li class="item_mark item_discount">-25%</li>
                                        <li class="item_mark item_new">new</li>
                                    </ul>
                                </div>
                            </div>

                            <!-- Recently Viewed Item -->
                            <div class="owl-item">
                                <div class="viewed_item is_new d-flex flex-column align-items-center justify-content-center text-center">
                                    <div class="viewed_image"><img src="{{asset('')}}images/view_4.jpg" alt=""></div>
                                    <div class="viewed_content text-center">
                                        <div class="viewed_price">$379</div>
                                        <div class="viewed_name"><a href="#">Huawei MediaPad...</a></div>
                                    </div>
                                    <ul class="item_marks">
                                        <li class="item_mark item_discount">-25%</li>
                                        <li class="item_mark item_new">new</li>
                                    </ul>
                                </div>
                            </div>

                            <!-- Recently Viewed Item -->
                            <div class="owl-item">
                                <div class="viewed_item discount d-flex flex-column align-items-center justify-content-center text-center">
                                    <div class="viewed_image"><img src="{{asset('')}}images/view_5.jpg" alt=""></div>
                                    <div class="viewed_content text-center">
                                        <div class="viewed_price">$225<span>$300</span></div>
                                        <div class="viewed_name"><a href="#">Sony PS4 Slim</a></div>
                                    </div>
                                    <ul class="item_marks">
                                        <li class="item_mark item_discount">-25%</li>
                                        <li class="item_mark item_new">new</li>
                                    </ul>
                                </div>
                            </div>

                            <!-- Recently Viewed Item -->
                            <div class="owl-item">
                                <div class="viewed_item d-flex flex-column align-items-center justify-content-center text-center">
                                    <div class="viewed_image"><img src="{{asset('')}}images/view_6.jpg" alt=""></div>
                                    <div class="viewed_content text-center">
                                        <div class="viewed_price">$375</div>
                                        <div class="viewed_name"><a href="#">Speedlink...</a></div>
                                    </div>
                                    <ul class="item_marks">
                                        <li class="item_mark item_discount">-25%</li>
                                        <li class="item_mark item_new">new</li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection
@section('someJS')

<script type="text/javascript">
 
    $('#tab3').on('click', function() {

        LoadChat('{{$product_edit->id}}');
    });
  function LoadChat(id){
        $('.ask').load("/ask/"+id, function(){
            $('#postComment').on('submit', function(){
        
            var comment = $('#comment').val(),
             UID = $('#uid').val(),
            TID = $('#uid').data("id");
            $.ajax({
        url: "{!! url('/postDiscuss') !!}",
        dataType: "json",
        type: "POST",
        data:{
          comment: comment,
          TID:TID,
           UID: UID,         
          _method:"post",
          _token : '{{ csrf_token() }}'
        }
      }).done(function(data){
                    if(data.success == true){
                        // alert('oke');
                        LoadChat();
                       
                    }else{
                       alert('failed');
                    }
            });
        });
    });
} 
$('#postQuestion').on('submit', function(){
        
        var question = $('#question').val(),
        UID = $('#uid').val(),
        PID = $('#uid').data("id");
    $.ajax({
        url: "{!! url('/postQuestion') !!}",
        dataType: "json",
        type: "POST",
        data:{
          question: question,
            PID:PID,
           UID: UID,         
          _method:"post",
          _token : '{{ csrf_token() }}'
        }
      }).done(function(data){
                    if(data.success == true){
                       
                        LoadChat();
                        $('#question').val("");
                    }else{
                       alert('failed');
                    }
            });
        });
$(function () {
   $('.panel-google-plus > .panel-footer > .input-placeholder, .panel-google-plus > .panel-google-plus-comment > .panel-google-plus-textarea > button[type="reset"]').on('click', function(event) {
        var $panel = $(this).closest('.panel-google-plus');
            $comment = $panel.find('.panel-google-plus-comment');
            
        $comment.find('.btn:first-child').addClass('disabled');
        $comment.find('textarea').val('');
        
        $panel.toggleClass('panel-google-plus-show-comment');
        
        if ($panel.hasClass('panel-google-plus-show-comment')) {
            $comment.find('textarea').focus();
        }
   });
   $('.panel-google-plus-comment > .panel-google-plus-textarea > textarea').on('keyup', function(event) {
        var $comment = $(this).closest('.panel-google-plus-comment');
        
        $comment.find('button[type="submit"]').addClass('disabled');
        if ($(this).val().length >= 1) {
            $comment.find('button[type="submit"]').removeClass('disabled');
        }
   });
});
</script>
@endsection