<footer class="footer">
  <div class="container">
    <div class="row">

      <div class="col-lg-3 footer_col">
        <div class="footer_column footer_contact">
          <div class="logo_container">
            <div class="logo"><a href="#">hariBelanja</a></div>
          </div>
          <div class="footer_title">Punya Pertanyaan? Hubungi</div>
          @php
          $footer = json_decode(\App\Settings::where('option', 'footer_contact')->first()->value);
  
          @endphp
         
          <div class="footer_phone">{{$footer->contact}}</div>
          <div class="footer_contact_text">
            <p>{{$footer->address}}</p>
            
          </div>
          <div class="footer_social">
            <ul>
              <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
              <li><a href="#"><i class="fab fa-twitter"></i></a></li>
              <li><a href="#"><i class="fab fa-youtube"></i></a></li>
              <li><a href="#"><i class="fab fa-google"></i></a></li>
              <li><a href="#"><i class="fab fa-vimeo-v"></i></a></li>
            </ul>
          </div>
        </div>
      </div>

      <div class="col-lg-2 offset-lg-2">
        <div class="footer_column">
          <div class="footer_title">Tentang hariBelanja</div>
          <ul class="footer_list">
            <li><a href="#">Cara Belanja</a></li>
            <li><a href="#">Cameras & Photos</a></li>
            <li><a href="#">Promo</a></li>
            <li><a href="#">Cara Berjualan</a></li>
            <li><a href="#">Keuntungan Jualan</a></li>
          </ul>
        </div>
      </div>

      <div class="col-lg-2">
        <div class="footer_column">
          <div class="footer_title">Customer Care</div>
          <ul class="footer_list">
            <li><a href="#">Akun Saya</a></li>
            <li><a href="#">Tracking Pesanan</a></li>
            <li><a href="#">Wish List</a></li>
            <li><a href="#">Returns / Exchange</a></li>
            <li><a href="#">FAQs</a></li>
          </ul>
        </div>
      </div>

    </div>
  </div>
</footer>
