@section('title','PAKO|HomePage')
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="description" content="OneTech shop project">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="{{asset('styles/bootstrap4/bootstrap.min.css')}}">
<link href="{{asset('plugins/fontawesome-free-5.0.1/css/fontawesome-all.css')}}" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="{{asset('plugins/OwlCarousel2-2.2.1/owl.carousel.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('plugins/OwlCarousel2-2.2.1/owl.theme.default.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('plugins/OwlCarousel2-2.2.1/animate.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('')}}plugins/jquery-ui-1.12.1.custom/jquery-ui.css">
<link rel="stylesheet" type="text/css" href="{{asset('')}}styles/shop_styles.css">
<link rel="stylesheet" type="text/css" href="{{asset('')}}styles/shop_responsive.css">
<link rel="stylesheet" type="text/css" href="{{asset('')}}styles/main_styles.css">

<style>
	.ml-left {
		margin-left: 18%!important;
	}
	.deals_item_name {
		font-size: 18px;
	}
	.deals_item_price {
		font-size: 20px;
	}
	.best_sellers {
    padding-bottom: 0px;
	}
</style>
