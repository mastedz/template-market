@section('title')
hariBelanja - Home
@endsection

@extends('frontend.layout_page')

@section('someCSS')
	<style>
		
	</style>
@endsection

@section('content')

	<div class="banner">
		<div class="banner_background" style="background-image:url(images/banner_background.jpg)"></div>
		<div class="container fill_height">
			<div class="row fill_height">
				<div class="banner_product_image"><img src="images/banner_product.png" alt=""></div>
				<div class="col-lg-5 offset-lg-4 fill_height">
					<div class="banner_content">
						<h1 class="banner_text">new era of smartphones</h1>
						<div class="banner_price"><span>Rp 530</span>Rp 460</div>
						<div class="banner_product_name">Apple Iphone 6s</div>
						<div class="button banner_button"><a href="#">Shop Now</a></div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Characteristics -->

	<div class="characteristics">
		<div class="container">
			<div class="row">

				<!-- Char. Item -->
				<div class="col-lg-4 col-md-6 char_col">

					<div class="char_item d-flex flex-row align-items-center justify-content-start">
						<div class="char_icon"><img src="images/char_1.png" alt=""></div>
						<div class="char_content">
							<div class="char_title">Jasa Pengiriman</div>
							<div class="char_subtitle">Pilihan jasa pengiriman dengan jangkauan nasional</div>
						</div>
					</div>
				</div>

				<!-- Char. Item -->
				<div class="col-lg-4 col-md-6 char_col">

					<div class="char_item d-flex flex-row align-items-center justify-content-start">
						<div class="char_icon"><img src="images/char_2.png" alt=""></div>
						<div class="char_content">
							<div class="char_title">Customer Support</div>
							<div class="char_subtitle">CS siap membantu Anda melalui e-mail dan call center (0822-4532-6737)</div>
						</div>
					</div>
				</div>

				<!-- Char. Item -->
				<div class="col-lg-4 col-md-6 char_col">

					<div class="char_item d-flex flex-row align-items-center justify-content-start">
						<div class="char_icon"><img src="images/char_3.png" alt=""></div>
						<div class="char_content">
							<div class="char_title">Pembayaran</div>
							<div class="char_subtitle">Menyediakan metode pembayaran untuk bertransaksi</div>
						</div>
					</div>
				</div>

				<!-- Char. Item
				<div class="col-lg-3 col-md-6 char_col">

					<div class="char_item d-flex flex-row align-items-center justify-content-start">
						<div class="char_icon"><img src="images/char_4.png" alt=""></div>
						<div class="char_content">
							<div class="char_title">Pembayaran</div>
							<div class="char_subtitle">Menyediakan metode pembayaran untuk bertransaksi</div>
						</div>
					</div>
				</div>-->
			</div>
		</div>
	</div>

	<!-- Deals of the week -->

	

@endsection

@section('someJS')
		
@endsection
