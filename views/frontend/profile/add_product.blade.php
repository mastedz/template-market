@section('title')
hariBelanja - Upload Barang
@endsection

  
@section('product_add')
hover-active
@endsection

@extends('frontend.layout_profile')

@section('content_div')
<style type="text/css">
    .btn-foto {
    text-align: center;
    position: relative;
    overflow: hidden;
}
.btn-foto input[type=file] {
    position: absolute;
    top: 0;
    right: 0;
    min-width: 100%;
    min-height: 100%;
    font-size: 100px;
    text-align: right;
    filter: alpha(opacity=0);
    opacity: 0;
    outline: none;   
    cursor: inherit;
    display: block;
}

  </style>

<form action="{{ route('products.store')}}" method="POST" id="contact_form" enctype="multipart/form-data">
  <div class="row" style="padding-right: 15px;padding-left: 15px;">
    <div class="col-md-12 order_total_content">
      <div class="list-name name-title">&nbsp;Upload Barang</div>
    </div>
    <div class="col-md-4">
      <ul class="cart_list" style="background:#e0e0e082;">
        <li class="cart_item clearfix cart_padding">
          <div class="order_total_content">
            <div id="preview1"  style="width: 100%;height:128px;">
              
            </div>
            <div class="cart_buttons margin-top-15">
              <span class="button cart_button_checkout width_100 btn-foto">
                  Browse <input class="upload" data-id="1" type="file" name="foto[]">
              </span>
            </div>
            <div>
              <p class="desc-foto">Besar file: maksimum 10.000.000 bytes (10 Megabytes) <br>Ekstensi file yang diperbolehkan: .JPG .JPEG .PNG</p>
            </div>
          </div>
        </li>
      </ul>
    </div>

    <div class="col-md-4">
      <ul class="cart_list" style="background:#e0e0e082;">
        <li class="cart_item clearfix cart_padding">
          <div class="order_total_content">
            <div id="preview2"  style="width: 100%;height:128px;">
              
            </div>
            <div class="cart_buttons margin-top-15">
              <span class="button cart_button_checkout width_100 btn-foto">
                  Browse <input class="upload" data-id="2" type="file" name="foto[]">
              </span>
            </div>
            <div>
              <p class="desc-foto">Besar file: maksimum 10.000.000 bytes (10 Megabytes) <br>Ekstensi file yang diperbolehkan: .JPG .JPEG .PNG</p>
            </div>
          </div>
        </li>
      </ul>
    </div>

    <div class="col-md-4">
      <ul class="cart_list" style="background:#e0e0e082;">
        <li class="cart_item clearfix cart_padding">
          <div class="order_total_content">
            <div id="preview3"  style="width: 100%;height:128px;">
              
            </div>
            <div class="cart_buttons margin-top-15">
              <span class="button cart_button_checkout width_100 btn-foto">
                  Browse <input class="upload" data-id="3" type="file" name="foto[]">
              </span>
            </div>
            <div>
              <p class="desc-foto">Besar file: maksimum 10.000.000 bytes (10 Megabytes) <br>Ekstensi file yang diperbolehkan: .JPG .JPEG .PNG</p>
            </div>
          </div>
        </li>
      </ul>
    </div>
    <br>
    </div>
  <div class="row">
    <div class="col-md-12">
      
       {{ @csrf_field()}}
        <div class="form-group row">
          <label for="inputEmail3" class="col-sm-2 col-form-label">Nama Product*</label>
          <div class="col-sm-10">
            <input type="text" name="nama" class="form-control" id="inputEmail3" placeholder="Nama Product">
          </div>
        </div>
        <div class="form-group row">
          <label for="inputEmail3" class="col-sm-2 col-form-label">Deskripsi</label>
          <div class="col-sm-10">
            <textarea type="text" name="description" class="form-control" id="inputEmail3" placeholder="Deskripsi barang"></textarea>
          </div>
        </div>
        <div class="form-group row">
          <label for="inputEmail3" class="col-sm-2 col-form-label">Kategori</label>
          <div class="col-sm-4">
            <select name="category" class="form-control" id="category">
              <option value="">Kategori</option>
              @foreach(App\Category::where('id_parent', NULL)->get() as $key => $value)
              <option class="{{$value->id_category}}" value="{{$value->id_category}}">{{$value->title}}</option>
              @endforeach
            </select>

          </div>
           <label for="inputEmail3" class="col-sm-2 col-form-label">Sub Kategori</label>
         <div class="col-sm-4">
            <select name="subcategory" class="form-control" id="subcategory">
              <option value="">Sub Kategori</option>
              @foreach(App\Category::where('id_parent','!=', NULL)->get() as $key => $value)
              <option class="{{$value->id_parent}}" value="{{$value->id_category}}">{{$value->title}}</option>
              @endforeach
            </select>
            
          </div>
        </div>
        <div class="form-group row">
          <label for="inputEmail3" class="col-sm-2 col-form-label">Harga</label>
          <div class="col-sm-10">
            <input name="harga" type="text" class="form-control" id="inputEmail3" placeholder="Harga">
          </div>
        </div>
        <div class="form-group row">
          <label for="inputEmail3" class="col-sm-2 col-form-label">Kondisi Barang</label>
          <div class="col-sm-10">
            <div class="form-check">
              <input class="form-check-input" type="radio" name="kondisi" id="exampleRadios1" value="1" checked>
              <label class="form-check-label" for="exampleRadios1">
                Baru
              </label>
            </div>
            <div class="form-check">
              <input class="form-check-input" type="radio" name="kondisi" id="exampleRadios1" value="0" checked>
              <label class="form-check-label" for="exampleRadios1">
                Bekas
              </label>
            </div>
          </div>
        </div>
        
        <div class="form-group row">
          <label for="inputEmail3" class="col-sm-2 col-form-label">Asuransi</label>
          <div class="col-sm-10">
            <div class="form-check">
              <input class="form-check-input" type="radio" name="asuransi" id="exampleRadios1" value="1" checked>
              <label class="form-check-label" for="exampleRadios1">
                ya
              </label>
            </div>
            <div class="form-check">
              <input class="form-check-input" type="radio" name="asuransi" id="exampleRadios1" value="0" checked>
              <label class="form-check-label" for="exampleRadios1">
                tidak
              </label>
            </div>
          </div>
        </div>
        <div class="form-group row">
          <label for="inputEmail3" class="col-sm-2 col-form-label">Berat</label>
          <div class="col-sm-10">
            <input type="number" name="weight" class="form-control" id="inputEmail3" placeholder="Berat">
          </div>
        </div>
        <div class="form-group row">
          <label for="inputEmail3" class="col-sm-2 col-form-label">Diskon</label>
          <div class="col-sm-10">
            <input type="number" name="discount" class="form-control" value="0" id="inputEmail3" placeholder="Diskon Barang">
          </div>
        </div>
        <div class="form-group row">
          <label for="inputEmail3" class="col-sm-2 col-form-label">Jumlah Barang</label>
          <div class="col-sm-10">
            <input type="number" name="qty" class="form-control" value="1" id="inputEmail3" placeholder="Jumlah Barang">
          </div>
        </div>
      
    </div>
  </div>
  <div class="order_total_content text-md-right margin-top-15" style="padding-right: 15px;padding-left: 15px;">
    <div class="contact_form_button">
      <input type="submit" class="button cart_button_checkout" value="simpan">
    </div>
  </div>
  </form>
@endsection
@section('someJS')
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-chained/1.0.1/jquery.chained.min.js"></script>
<script type="text/javascript">
 $(document).ready(function(){

      $("#subcategory").chainedTo("#category");
   
    $(".upload").on('change',function(){
      console.log($(this).data('id'));
      var filereader = new FileReader();
      var $img=jQuery.parseHTML("<img style='width: 100%;height:128px;' src=''>");
      filereader.onload = function(){
          $img[0].src=this.result;
      };
      filereader.readAsDataURL(this.files[0]);
       $("#preview" + $(this).data("id")).empty();
      $("#preview" + $(this).data("id")).append($img);
    });
  });
</script>
@endsection