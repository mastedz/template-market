@section('title')
hariBelanja - Product
@endsection

@section('product')
hover-active
@endsection

@extends('frontend.layout_profile')

@section('css')
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.css">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-css/1.4.6/select2-bootstrap.css">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.css">
    <style>
    /*
      loader
    */

    #loader {
          bottom: 0;
          height: 175px;
          left: 0;
          margin: auto;
          position: absolute;
          right: 0;
          top: 0;
          width: 175px;
      }
      #loader .dot {
          bottom: 0;
          height: 100%;
          left: 0;
          margin: auto;
          position: absolute;
          right: 0;
          top: 0;
          width: 87.5px;
      }
      #loader .dot::before {
          border-radius: 100%;
          content: "";
          height: 87.5px;
          left: 0;
          position: absolute;
          right: 0;
          top: 0;
          transform: scale(0);
          width: 87.5px;
      }
      #loader .dot:nth-child(7n+1) {
          transform: rotate(45deg);
      }
      #loader .dot:nth-child(7n+1)::before {
          animation: 0.8s linear 0.1s normal none infinite running load;
          background: #00ff80 none repeat scroll 0 0;
      }
      #loader .dot:nth-child(7n+2) {
          transform: rotate(90deg);
      }
      #loader .dot:nth-child(7n+2)::before {
          animation: 0.8s linear 0.2s normal none infinite running load;
          background: #00ffea none repeat scroll 0 0;
      }
      #loader .dot:nth-child(7n+3) {
          transform: rotate(135deg);
      }
      #loader .dot:nth-child(7n+3)::before {
          animation: 0.8s linear 0.3s normal none infinite running load;
          background: #00aaff none repeat scroll 0 0;
      }
      #loader .dot:nth-child(7n+4) {
          transform: rotate(180deg);
      }
      #loader .dot:nth-child(7n+4)::before {
          animation: 0.8s linear 0.4s normal none infinite running load;
          background: #0040ff none repeat scroll 0 0;
      }
      #loader .dot:nth-child(7n+5) {
          transform: rotate(225deg);
      }
      #loader .dot:nth-child(7n+5)::before {
          animation: 0.8s linear 0.5s normal none infinite running load;
          background: #2a00ff none repeat scroll 0 0;
      }
      #loader .dot:nth-child(7n+6) {
          transform: rotate(270deg);
      }
      #loader .dot:nth-child(7n+6)::before {
          animation: 0.8s linear 0.6s normal none infinite running load;
          background: #9500ff none repeat scroll 0 0;
      }
      #loader .dot:nth-child(7n+7) {
          transform: rotate(315deg);
      }
      #loader .dot:nth-child(7n+7)::before {
          animation: 0.8s linear 0.7s normal none infinite running load;
          background: magenta none repeat scroll 0 0;
      }
      #loader .dot:nth-child(7n+8) {
          transform: rotate(360deg);
      }
      #loader .dot:nth-child(7n+8)::before {
          animation: 0.8s linear 0.8s normal none infinite running load;
          background: #ff0095 none repeat scroll 0 0;
      }
      #loader .lading {
          background-image: url("");
          background-position: 50% 50%;
          background-repeat: no-repeat;
          bottom: -40px;
          height: 20px;
          left: 0;
          position: absolute;
          right: 0;
          width: 180px;
      }
      @keyframes load {
      100% {
          opacity: 0;
          transform: scale(1);
      }
      }
      @keyframes load {
      100% {
          opacity: 0;
          transform: scale(1);
      }
      }
    </style>
    <style>
      /* The container */
      .container-radio {
      display: block;
      position: relative;
      padding-left: 0px;
      margin-bottom: 12px;
      cursor: pointer;
      font-size: 22px;
      -webkit-user-select: none;
      -moz-user-select: none;
      -ms-user-select: none;
      user-select: none;
      }

      /* Hide the browser's default radio button */
      .container-radio input {
      position: absolute;
      opacity: 0;
      cursor: pointer;
      }

      /* Create a custom radio button */
      .checkmark {
      position: absolute;
      top: 0;
      left: 0;
      height: 15px;
      width: 15px;
      background-color: #eee;
      border-radius: 50%;
      }

      /* On mouse-over, add a grey background color */
      .container-radio:hover input ~ .checkmark {
      background-color: #ccc;
      }

      /* When the radio button is checked, add a blue background */
      .container-radio input:checked ~ .checkmark {
      background-color: #2196F3;
      }

      /* Create the indicator (the dot/circle - hidden when not checked) */
      .checkmark:after {
      content: "";
      position: absolute;
      display: none;
      }

      /* Show the indicator (dot/circle) when checked */
      .container-radio input:checked ~ .checkmark:after {
      display: block;
      }

      /* Style the indicator (dot/circle) */
      .container-radio .checkmark:after {
        top: 4px;
        left: 4px;
        width: 8px;
        height: 8px;
        border-radius: 50%;
        background: white;
      }
    </style>

  <style>
    .urutkan{
      margin-right: 20px;
      padding-top: 14px;
    }
    .btn-urutkan{
      margin: 2px 0px 3px 0px;
      background: #e6e6e67a;
      border-radius: 5px !important;
      color: #7f8484;
    }
    .table{
      margin-top: 15px;
      font-size: 12px;
      color: #7f8484;
    }
    .border-btn{
      border-color: #929292;
      font-size: 12px;
      background: #ffff;
    }
  </style>

  <style>
		.cd-user-modal {
			position: fixed;
			top: 0;
			left: 0;
			width: 100%;
			height: 100%;
			background: rgba(52, 54, 66, 0.9);
			z-index: 3;
			overflow-y: auto;
			cursor: pointer;
			visibility: hidden;
			opacity: 0;
			-webkit-transition: opacity 0.3s 0, visibility 0 0.3s;
			-moz-transition: opacity 0.3s 0, visibility 0 0.3s;
			transition: opacity 0.3s 0, visibility 0 0.3s;
		}
		.cd-user-modal.is-visible {
			z-index: 99;
			visibility: visible;
			opacity: 1;
			-webkit-transition: opacity 0.3s 0, visibility 0 0;
			-moz-transition: opacity 0.3s 0, visibility 0 0;
			transition: opacity 0.3s 0, visibility 0 0;
		}
		.cd-user-modal.is-visible .cd-user-modal-container {
			-webkit-transform: translateY(0);
			-moz-transform: translateY(0);
			-ms-transform: translateY(0);
			-o-transform: translateY(0);
			transform: translateY(0);
		}

		.cd-user-modal-container {
			position: relative;
			width: 90%;
			max-width: 600px;
			background: #fff;
			margin: 3em auto 4em;
			cursor: auto;
			border-radius: 0.25em;
			-webkit-transform: translateY(-30px);
			-moz-transform: translateY(-30px);
			-ms-transform: translateY(-30px);
			-o-transform: translateY(-30px);
			transform: translateY(-30px);
			-webkit-transition-property: -webkit-transform;
			-moz-transition-property: -moz-transform;
			transition-property: transform;
			-webkit-transition-duration: 0.3s;
			-moz-transition-duration: 0.3s;
			transition-duration: 0.3s;
		}
		.cd-user-modal-container .cd-switcher::after {
			clear: both;
			content: "";
			display: table;
		}
		.cd-user-modal-container .cd-switcher li {
			width: 50%;
			float: left;
			text-align: center;
		}
		.cd-user-modal-container .cd-switcher li:first-child a {
			border-radius: 0.25em 0 0 0;
		}
		.cd-user-modal-container .cd-switcher li:last-child a {
			border-radius: 0 0.25em 0 0;
		}
		.cd-user-modal-container .cd-switcher a {
			display: block;
			width: 100%;
			height: 50px;
			line-height: 50px;
			background: #b6bfd08a;
			color: #809191;
		}
		.cd-user-modal-container .cd-switcher a.selected {
			background: #fff;
			color: #505260;
		}
		@media only screen and (min-width: 600px) {
			.cd-user-modal-container {
				margin: 4em auto;
			}
			.cd-user-modal-container .cd-switcher a {
				height: 70px;
				line-height: 70px;
			}
		}

		.cd-form {
			padding: 1.4em;
		}
		.cd-form .fieldset {
			position: relative;
			margin: 1.4em 0;
		}
		.cd-form .fieldset:first-child {
			margin-top: 0;
		}
		.cd-form .fieldset:last-child {
			margin-bottom: 0;
		}
		.cd-form label {
			font-size: 14px;
			font-size: 0.875rem;
		}
		.cd-form label.image-replace {
			/* replace text with an icon */
			display: inline-block;
			position: absolute;
			left: 15px;
			top: 50%;
			bottom: auto;
			-webkit-transform: translateY(-50%);
			-moz-transform: translateY(-50%);
			-ms-transform: translateY(-50%);
			-o-transform: translateY(-50%);
			transform: translateY(-50%);
			height: 20px;
			width: 20px;
			overflow: hidden;
			text-indent: 100%;
			white-space: nowrap;
			color: transparent;
			text-shadow: none;
			background-repeat: no-repeat;
			background-position: 50% 0;
		}
		.cd-form label.cd-username {
			background-image: url("https://s3-us-west-2.amazonaws.com/s.cdpn.io/148866/cd-icon-username.svg");
		}
		.cd-form label.cd-email {
			background-image: url("https://s3-us-west-2.amazonaws.com/s.cdpn.io/148866/cd-icon-email.svg");
		}
		.cd-form label.cd-password {
			background-image: url("https://s3-us-west-2.amazonaws.com/s.cdpn.io/148866/cd-icon-password.svg");
		}
		.cd-form input {
			margin: 0;
			padding: 0;
			border-radius: 0.25em;
		}
		.cd-form input.full-width {
			width: 100%;
		}
		form-control {
			padding: 12px 20px 12px 10px;
		}
		.cd-form input.has-border {
			border: 1px solid #d2d8d8;
			-webkit-appearance: none;
			-moz-appearance: none;
			-ms-appearance: none;
			-o-appearance: none;
			appearance: none;
		}
		.cd-form input.has-border:focus {
			border-color: #343642;
			box-shadow: 0 0 5px rgba(52, 54, 66, 0.1);
			outline: none;
		}
		.cd-form input.has-error {
			border: 1px solid #d76666;
		}
		.cd-form input[type="password"] {
			/* space left for the HIDE button */
			padding-right: 65px;
		}
		.cd-form input[type="submit"] {
			padding: 16px 0;
			cursor: pointer;
			background: #0e8ce4;
			color: #fff;
			font-weight: bold;
			border: none;
			-webkit-appearance: none;
			-moz-appearance: none;
			-ms-appearance: none;
			-o-appearance: none;
			appearance: none;
		}
    .cd-form input[type="button"] {
			padding: 16px 0;
			cursor: pointer;
			background: #0e8ce4;
			color: #fff;
			font-weight: bold;
			border: none;
			-webkit-appearance: none;
			-moz-appearance: none;
			-ms-appearance: none;
			-o-appearance: none;
			appearance: none;
		}
		.no-touch .cd-form input[type="submit"]:hover, .no-touch .cd-form input[type="submit"]:focus {
			background: #3599ae;
			outline: none;
		}
    .no-touch .cd-form input[type="button"]:hover, .no-touch .cd-form input[type="button"]:focus {
			background: #3599ae;
			outline: none;
		}
		.cd-form .hide-password {
			display: inline-block;
			position: absolute;
			right: 0;
			top: 0;
			padding: 6px 15px;
			border-left: 1px solid #d2d8d8;
			top: 50%;
			bottom: auto;
			-webkit-transform: translateY(-50%);
			-moz-transform: translateY(-50%);
			-ms-transform: translateY(-50%);
			-o-transform: translateY(-50%);
			transform: translateY(-50%);
			font-size: 14px;
			font-size: 0.875rem;
			color: #343642;
		}
		.cd-form .cd-error-message {
			display: inline-block;
			position: absolute;
			left: -5px;
			bottom: -35px;
			background: rgba(215, 102, 102, 0.9);
			padding: 0.8em;
			z-index: 2;
			color: #fff;
			font-size: 13px;
			font-size: 0.8125rem;
			border-radius: 0.25em;
			/* prevent click and touch events */
			pointer-events: none;
			visibility: hidden;
			opacity: 0;
			-webkit-transition: opacity 0.2s 0, visibility 0 0.2s;
			-moz-transition: opacity 0.2s 0, visibility 0 0.2s;
			transition: opacity 0.2s 0, visibility 0 0.2s;
		}
		.cd-form .cd-error-message::after {
			/* triangle */
			content: "";
			position: absolute;
			left: 22px;
			bottom: 100%;
			height: 0;
			width: 0;
			border-bottom: 8px solid rgba(215, 102, 102, 0.9);
			border-left: 8px solid transparent;
			border-right: 8px solid transparent;
		}
		.cd-form .cd-error-message.is-visible {
			opacity: 1;
			visibility: visible;
			-webkit-transition: opacity 0.2s 0, visibility 0 0;
			-moz-transition: opacity 0.2s 0, visibility 0 0;
			transition: opacity 0.2s 0, visibility 0 0;
		}
		@media only screen and (min-width: 600px) {
			.cd-form {
				padding: 2em;
			}
			.cd-form .fieldset {
				margin: 2em 0;
			}
			.cd-form .fieldset:first-child {
				margin-top: 0;
			}
			.cd-form .fieldset:last-child {
				margin-bottom: 0;
			}
			.cd-form input.has-padding {
				padding: 16px 20px 16px 10px;
			}
			.cd-form input[type="submit"] {
				padding: 16px 0;
			}
      .cd-form input[type="button"] {
				padding: 16px 0;
			}
		}

		.cd-form-message {
			padding: 1.4em 1.4em 0;
			font-size: 14px;
			font-size: 0.875rem;
			line-height: 1.4;
			text-align: center;
		}
		@media only screen and (min-width: 600px) {
			.cd-form-message {
				padding: 2em 2em 0;
			}
		}

		.cd-form-bottom-message {
			position: absolute;
			width: 100%;
			left: 0;
			bottom: -50px;
			text-align: center;
			font-size: 14px;
			font-size: 0.875rem;
		}
		.cd-form-bottom-message a {
			color: #fff;
			text-decoration: underline;
		}

		.cd-close-form {
			/* form X button on top right */
			display: block;
			position: absolute;
			width: 40px;
			height: 40px;
			right: 0;
			top: -40px;
			background: url("https://s3-us-west-2.amazonaws.com/s.cdpn.io/148866/cd-icon-close.svg") no-repeat center center;
			text-indent: 100%;
			white-space: nowrap;
			overflow: hidden;
		}
		@media only screen and (min-width: 1170px) {
			.cd-close-form {
				display: none;
			}
		}

		#cd-login,
		#cd-signup,
		#cd-reset-password {
			display: none;
		}

		#cd-login.is-selected,
		#cd-signup.is-selected,
		#cd-reset-password.is-selected {
			display: block;
		}
    .margin-button-5{
      margin-bottom: 5px !important;
      margin-top: 5px !important;
    }
    .p-margin-button-0{
      margin-bottom: 0px !important;
    }
    .margin-left{
      margin-left: 0px;
    }
    textarea{
      padding: 16px 20px 16px 10px;
      border: 1px solid #d2d8d8;
      border-radius: 0.25em;
      width: 100%;
    }

    .select2-container{
      width: 100% !important;
    }
    .select2-container--default .select2-selection--single{
      padding: 11px 20px 33px 10px;
    }

	</style>
@endsection

@section('content_div')

  <div class="row" style="padding-right: 15px;padding-left: 15px;">
    <div class="col-md-12 order_total_content">
      <div class="list-name name-title">&nbsp;Daftar Product</div>
      <div class="order_total_content text-md-right margin-top-15">
        <div class="contact_form_button">
          <!-- <button type="button" class="button cart_button_checkout float-left"><i class="fas fa-plus"></i> Tambah Product</button> -->
          <a class="cd-signup button cart_button_checkout float-left" href="#0"><i class="fas fa-plus"></i> Tambah Product</a>
          <div class="btn-group">
            <p class="urutkan">Urutkan</p>
            <button class="btn btn-default btn-sm dropdown-toggle btn-urutkan" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Product Terbaru
            </button>
            <div class="dropdown-menu">
              <a class="dropdown-item" href="#">Product Terbaru</a>
              <a class="dropdown-item" href="#">Nama Product</a>
            </div>
          </div>
        </div>
      </div>
      <!--Table-->
      <table class="table table-hover table-fixed">

        <!--Table head-->
        <thead>
          <tr>
            
            <th>Nama</th>
            <th>Harga Satuan</th>
            <th>Qty</th>
            <th>Diskon</th>
            <th>Kategori</th>
            <th>Action</th>
          </tr>
        </thead>
        <!--Table head-->

        <!--Table body-->
        <tbody>
          @foreach($products as $product)
            <tr>
              
              <td>{{ $product->name }}</td>
              <td>@if($product->qty == null){{ "0" }}@else {{$product->qty}} @endif</td>
              <td>{{ $product->category->name }}</td>
              <td>
                <button type="button" class="btn btn-default btn-sm btn-urutkan border-btn opeditedit" data-id="{{ $product->id }}"><i class="fas fa-edit"></i> Ubah</button>&nbsp;&nbsp;
                <button type="button" class="btn btn-default btn-sm btn-urutkan border-btn opendelete" data-id="{{ $product->id }}"><i class="fas fa-trash-alt"></i> Hapus</button>
              </td>
            </tr>
          @endforeach
          <!-- <tr>
            <th scope="row">
              <label class="container-radio">
                <input type="radio" name="radio">
                <span class="checkmark"></span>
              </label>
            </th>
            <td><b>Cuk Gengs</b><br>082245326737</td>
            <td><b>Kantor</b><br>JL. Tabanas No. 8a/9</td>
            <td>DKI Jakarta,<br> Kota Administrasi Jakarta Selatan,<br> Pancoran 12870<br>Indonesia</td>
            <td><a href="#"  class="btn btn-default btn-sm btn-urutkan border-btn"><i class="fas fa-edit"></i> Ubah</a>&nbsp;&nbsp;<a href="#"  class="btn btn-default btn-sm btn-urutkan border-btn"><i class="fas fa-trash-alt"></i> Hapus</a></td>
          </tr> -->
        </tbody>
        <!--Table body-->

      </table>
      <!--Table-->
    </div>
  </div>
  
    <!-- <div id="loader">
			<div class="dot"></div>
			<div class="dot"></div>
			<div class="dot"></div>
			<div class="dot"></div>
			<div class="dot"></div>
			<div class="dot"></div>
			<div class="dot"></div>
			<div class="dot"></div>
			<div class="lading"></div>
		</div> -->
@endsection

@section('someJS')
  {{--  <script src="https://code.jquery.com/jquery-3.1.1.min.js" integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" type="text/javascript"></script>

		<script type="text/javascript">
			$( document ).ready(function() {
        $('#loader').hide();
        $("#disease").select2({
             allowClear:true,
             placeholder: 'Search for a disease'
        });

			var $form_modal = $('.cd-user-modal'),
				$form_login = $form_modal.find('#cd-login'),
				$form_signup = $form_modal.find('#cd-edit'),
				$form_forgot_password = $form_modal.find('#cd-reset-password'),
				$form_modal_tab = $('.cd-switcher'),
				$tab_login = $form_modal_tab.children('li').eq(0).children('a'),
				$tab_signup = $form_modal_tab.children('li').eq(1).children('a'),
				$forgot_password_link = $form_login.find('.cd-form-bottom-message a'),
				$back_to_login_link = $form_forgot_password.find('.cd-form-bottom-message a'),
				$main_nav = $('.main-nav');

			//open modal
			$('.cd-signup').on('click', function(event){


					// on mobile open the submenu
				$form_modal.addClass('is-visible');
				$('#btn-update').data('id', 'add');
				signup_selected();


			});
			$('.cd-signin').on('click', function(event){


					// on mobile open the submenu
				$form_modal.addClass('is-visible');
				login_selected();


			});


			//close modal
			$('.cd-user-modal').on('click', function(event){
				if( $(event.target).is($form_modal) || $(event.target).is('.cd-close-form') ) {
					$form_modal.removeClass('is-visible');
				}
			});
			//close modal when clicking the esc keyboard button
			$(document).keyup(function(event){
					if(event.which=='27'){
						$form_modal.removeClass('is-visible');
					}
				});

			//switch from a tab to another
			$form_modal_tab.on('click', function(event) {
				event.preventDefault();
				( $(event.target).is( $tab_login ) ) ? login_selected() : signup_selected();
			});

			//hide or show password
			$('.hide-password').on('click', function(){
				var $this= $(this),
					$password_field = $this.prev('input');

				( 'password' == $password_field.attr('type') ) ? $password_field.attr('type', 'text') : $password_field.attr('type', 'password');
				( 'Hide' == $this.text() ) ? $this.text('Show') : $this.text('Hide');
				//focus and move cursor to the end of input field
				$password_field.putCursorAtEnd();
			});

			//show forgot-password form
			$forgot_password_link.on('click', function(event){
				event.preventDefault();
				forgot_password_selected();
			});

			//back to login from the forgot-password form
			$back_to_login_link.on('click', function(event){
				event.preventDefault();
				login_selected();
			});

			function login_selected(){
				$form_login.addClass('is-selected');
				$form_signup.removeClass('is-selected');
				$form_forgot_password.removeClass('is-selected');
				$tab_login.addClass('selected');
				$tab_signup.removeClass('selected');
			}

			function signup_selected(){
				$form_login.removeClass('is-selected');
				$form_signup.addClass('is-selected');
				$form_forgot_password.removeClass('is-selected');
				$tab_login.removeClass('selected');
				$tab_signup.addClass('selected');
			}

			function forgot_password_selected(){
				$form_login.removeClass('is-selected');
				$form_signup.removeClass('is-selected');
				$form_forgot_password.addClass('is-selected');
			}

			//REMOVE THIS - it's just to show error messages
			$form_login.find('input[type="submit"]').on('click', function(event){
				event.preventDefault();
				$form_login.find('input[type="email"]').toggleClass('has-error').next('span').toggleClass('is-visible');
			});
			$form_signup.find('input[type="submit"]').on('click', function(event){
				event.preventDefault();
				$form_signup.find('input[type="email"]').toggleClass('has-error').next('span').toggleClass('is-visible');
			});

			$('#btn-simpan').on('click', function(){
				if ($(this).data('id')=='add') {
					console.log("hai");
					addData();
				}else{
					updateData();
				}


			})
			//IE9 placeholder fallback
			//credits http://www.hagenburger.net/BLOG/HTML5-Input-Placeholder-Fix-With-jQuery.html
			if(!Modernizr.input.placeholder){
				$('[placeholder]').focus(function() {
					var input = $(this);
					if (input.val() == input.attr('placeholder')) {
						input.val('');
						}
				}).blur(function() {
					var input = $(this);
						if (input.val() == '' || input.val() == input.attr('placeholder')) {
						input.val(input.attr('placeholder'));
						}
				}).blur();
				$('[placeholder]').parents('form').submit(function() {
						$(this).find('[placeholder]').each(function() {
						var input = $(this);
						if (input.val() == input.attr('placeholder')) {
							input.val('');
						}
						})
				});
			}

		});


		//credits https://css-tricks.com/snippets/jquery/move-cursor-to-end-of-textarea-or-input/
		jQuery.fn.putCursorAtEnd = function() {
			return this.each(function() {
					// If this function exists...
					if (this.setSelectionRange) {
							// ... then use it (Doesn't work in IE)
							// Double the length because Opera is inconsistent about whether a carriage return is one character or two. Sigh.
							var len = $(this).val().length * 2;
							this.setSelectionRange(len, len);
					} else {
						// ... otherwise replace the contents with itself
						// (Doesn't work in Google Chrome)
							$(this).val($(this).val());
					}
			});
		};

		//ajax add address
		function addData() {


      $('#loader').show();
      var addressName  = $('#address-name').val();
			var receiverName = $('#receiver-name').val();
			var phone        = $('#phone').val();
      var city         = $('#city').val();
			var zipcode      = $('#zipcode').val();
      var address      = $('#address').val();
      // alert('addressName : '+addressName+', receiverName : '+receiverName+', phone : '+phone+', city : '+city+', zipcode : '+zipcode+', address : '+address);

      if(addressName != '' && receiverName != '' && phone != '' && city != '' && zipcode != '' && address != ''){
        $.ajax({
          url: "{!! url('/store') !!}",
          dataType: "json",
          type: "POST",
          data:{
            addressName: addressName,
  					receiverName: receiverName,
  					phone: phone,
            city: city,
            zipcode: zipcode,
  					address: address,
            _method:"post",
            _token : '{{ csrf_token() }}'
          }
        }).done(function(data){
          $('#loader').hide();
  				if(data.resultCode == '00'){
  					alert(data.resultDesc);
            window.location.reload();
            $('#loader').show();
  				}else{
  					alert(data.resultDesc);
            window.location.reload();
            $('#loader').show();
  				}
  			});
      }else{
        alert('data belum lengkap');
      }
	}

    //edit address
    $('.opeditedit').off().on('click', function(){
      var id = $(this).attr('data-id');
      $.ajax({
        url: "{!! url('/alamat/detail') !!}",
        dataType: "json",
        type: "POST",
        data:{
          id: id,
          _method:"post",
          _token : '{{ csrf_token() }}'
        }
      }).done(function(data){
      	console.log(data);
          $('.cd-user-modal').addClass('is-visible');
          $('#btn-update').data('id', 'update');
          $('#edit-address-name').val(data.name);
					$('#edit-receiver-name').val(data.receiver);
					$('#edit-phone').val(data.phoneno);
					$('#edit-city').val(data.state+'&'+data.id);
					$('#edit-zipcode').val(data.postcode);
					$('#edit-address').val(data.address);
					$('#edit-id').val(data.id);
      });
    });

    //update
    function updateData(){
      var addressName  = $('#edit-address-name').val();
			var receiverName = $('#edit-receiver-name').val();
			var phone        = $('#edit-phone').val();
      var city         = $('#edit-city').val();
			var zipcode      = $('#edit-zipcode').val();
      var address      = $('#edit-address').val();
      var id           = $('#edit-id').val();

      // alert(addressName+receiverName+phone+city+zipcode+address+id);

      $.ajax({
        url: "{!! url('/alamat/update') !!}",
        dataType: "json",
        type: "POST",
        data:{
          id: id,
          addressName: addressName,
          receiverName: receiverName,
          phone: phone,
          city: city,
          zipcode: zipcode,
          address: address,
          _method:"post",
          _token : '{{ csrf_token() }}'
        }
      }).done(function(data){
      	console.log(data);
        if(data.resultCode == '00'){
          alert(data.resultDesc);
          window.location.reload();
        }else{
          alert(data.resultDesc);
          window.location.reload();
        }
      });
   }
   function resetInput(argument) {
   	 var addressName  = $('#edit-address-name').val(""),
	receiverName = $('#edit-receiver-name').val(""),
	 phone        = $('#edit-phone').val(""),
     city         = $('#edit-city').val(""),
	 zipcode      = $('#edit-zipcode').val(""),
     address      = $('#edit-address').val(""),
     id           = $('#edit-id').val("");

   }
    //delete
    $('.opendelete').off().on('click', function(){
      alert('sip');
      var id = $(this).attr('data-id');
      $.ajax({
        url: "{!! url('/alamat/delete/') !!}"+'/'+id,
        dataType: "json",
        type: "GET",
      }).done(function(data){
      	console.log(data);
        if(data.resultCode == '00'){
          alert('alamat berhasil di delete');
          window.location.reload();
        }else{
          alert(data.resultDesc);
          window.location.reload();
        }

      });
    });
		</script>--}}
@endsection
