@section('title')
hariBelanja - Biodata
@endsection

@section('biodata')
hover-active
@endsection

@extends('frontend.layout_profile')

@section('content_div')

  <div class="row" style="padding-right: 15px;padding-left: 15px;">
    <div class="col-md-12 order_total_content">
      <div class="list-name name-title">&nbsp;Ubah Biodata Diri</div>
    </div>
    <!-- <form role="form" action="{{ url('/update/profile') }}" method="post" enctype="multipart/form-data" class="f1"> -->
    <div class="col-md-4">

      <form role="form" action="{{ url('/update/profile') }}" method="post" enctype="multipart/form-data" class="f1">
        {{ csrf_field() }}
      <ul class="cart_list" style="background:#e0e0e082;">
        <li class="cart_item clearfix cart_padding">
          <div class="order_total_content">
            <div>
              @if(!empty($user->image))
                <img src="{{ asset('/storage/images/'.$user->image) }}" alt="HTML5 Icon" style="width: 100%;height:auto;">
              @else
                <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQYm-KcyvHy3PDkmh0V9KzkUk26255h0RwthshiaoanTnfH2B_IRg" alt="HTML5 Icon" style="width: 100%;height:auto;">
              @endif
            </div>
            <div class="cart_buttons margin-top-15">
              <button type="button" class="button cart_button_checkout width_100 btn-foto" onclick="document.getElementById('getFile').click()">Pilih Foto</button>
              <input name="image" type='file' id="getFile" style="display:none">
            </div>
            <div>
              <p class="desc-foto">Besar file: maksimum 10.000.000 bytes (10 Megabytes) <br>Ekstensi file yang diperbolehkan: .JPG .JPEG .PNG</p>
            </div>
          </div>
        </li>
      </ul>
    </div>
    <div class="col-md-8">
      <!-- <form action="#" id="contact_form"> -->
        <div class="contact_form_inputs d-flex flex-md-row flex-column justify-content-between align-items-between">
          <input type="text" id="contact_form_name" class="contact_form_name input_field width_100" placeholder="Isi Nama" required="required" data-error="Name is required." value="{{ $user->name }}" name="name">
        </div>
        <div class="contact_form_inputs d-flex flex-md-row flex-column justify-content-between align-items-between">
          <input type="text" id="contact_form_email" class="contact_form_email input_field width_100" placeholder="Isi Email" required="required" data-error="Email is required." value="{{ $user->email }}" disabled name="email">
        </div>
        <div class="contact_form_inputs d-flex flex-md-row flex-column justify-content-between align-items-between">
          <input type="text" id="contact_form_phone" class="contact_form_phone input_field width_100" placeholder="Isi Nomor Telpon" name="phone" value="{{ $user->phone }}">
        </div>
      <!-- </form> -->
    </div>
  </div>
  <div class="order_total_content text-md-right margin-top-15" style="padding-right: 15px;padding-left: 15px;">
    <div class="contact_form_button">
      <button type="button" class="button cart_button_checkout float-left" style="background:#b5b6b785;"><i class="fas fa-key"></i> Ubah Password</button>
      <button type="submit" class="button cart_button_checkout">Simpan</button>
    </div>
  </div>
</form>
@endsection
