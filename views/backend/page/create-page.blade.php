@extends('backend.includes.header')

@section('content')
  <div class="right_col" role="main">
    
    <div class="">
      <div class="row">
        <div class="col-md-12">
          <div class="x_panel">
            <div class="x_title">
              <h3>Create new page</h3>
              <div class="clearfix">
              </div>
            </div>
            <form id="create_category_form" action="{{ route('admin.page.store') }}" method="post">
              <div class="x_content">
                <div class="table-responsive">
                  <div class="col-md-9">
                    <div class="col-md-6">
                      <h4>Title</h4>
                      <input type="text" class="form-control" placeholder="Title" name="title">
                    </div>

                    <div class="col-md-12 col-sm-12 col-xs-12" style="padding-top:25px">
                      <h2>Description</h2>
                      <div class="x_content">
                        <div id="alerts"></div>
                        <textarea class="content" id="content" name="description"></textarea>
                      </div>
                    </div>

                  </div>
                  <div class="col-md-3">
                    <div class="container x_panel" style="background-color:#d9edf7" >
                      <div class="btn-group " style="width:100%">
                        <p>Schema</p>
                        <select class="form-control btn-primary" name="schema">
                          <option value="WebPage">WebPage</option>
                          <option value="AboutPage">AboutPage</option>
                          <option value="ContactPage">ContactPage</option>
                          <option value="QAPage">QAPage</option>
                          <option value="SearchResultPage">SearchResultPage</option>
                        </select>

                      </div><br><br>
                      <div class="">
                        <p>Meta Title</p>
                        <input type="text" class="form-control" placeholder="Meta Title" name="meta_title">
                      </div><br>
                      <div class="">
                        <p>Meta Description</p>
                        <textarea class="form-control" rows="3" placeholder="Meta Description" name="meta_description"></textarea>
                      </div><br>
                      <div class="">
                        <p>Meta Keywords</p>
                        <input type="text" class="form-control" placeholder="Meta Keywords" name="meta_keyword">
                      </div><br>
                      <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <button class="btn btn-primary" type="reset">Reset</button>
                          <button type="submit" class="btn btn-success">Submit</button>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
  <script type="text/javascript">
    var CREATE_CATEGORY_SUCCESS_LINK = "{{ route('admin.page.index') }}";
  </script>
@endsection
