@extends('backend.includes.header')

@section('content')
  <div class="right_col" role="main">
    <div class="">
      <div class="row">
        <div class="col-md-12">
          <div class="x_panel">
            <div class="x_title">
              Main Menu
              <a href="{{ route('admin.menu.create') }}" class="btn btn-success pull-right"><i class="fa fa-plus" aria-hidden="true"></i> Create</a>
              <div class="clearfix">
              </div>
            </div>
            <div class="x_content">
              <style media="screen">
              .a-center {
                  width: 50px;
              }

              .table tbody tr.child td:nth-child(2) {
                padding: 8px 0 8px 30px;
              }

              .table tbody tr.child-child td:nth-child(2) {
                padding:8px 0 8px 60px;
              }

              .table tbody tr.child-child-child td:nth-child(2) {
                padding:8px 0 8px 90px;
              }

              .table>tbody+tbody {
                border-top: 0.5px solid #ddd;
              }
              .parent {
                cursor: pointer;
              }
              .child{
                display: none;
              }
              .child-child {
                display: none;
              }
              .child-child-child {
                display: none;
              }
              </style>
              <div class="table-responsive">
                <table class="table jambo_table bulk_action">
                  <thead>
                    <tr class="headings">
                      <th>
                      </th>
                      <th class="column-title">Title </th>
                      <th class="column-title">URL </th>
                      <th class="column-title no-link last" width="100px"><span class="nobr">Action</span>
                      </th>
                      <th class="bulk-actions" colspan="9">
                        <a class="antoo" style="color:#fff; font-weight:500;">Bulk Actions ( <span class="action-cnt"> </span> ) <i class="fa fa-chevron-down"></i></a>
                      </th>
                    </tr>
                  </thead>

                  <tbody>
                    @foreach ($menus as $idx => $m)
                    @php
                      $parent_index = $loop->index;
                    @endphp
                    <tr class="even pointer">
                      <td class="a-center ">
                      </td>
                      <td class="parent" child='{{ $loop->index }}' colspan="2">{{ ucwords($idx) }} Menu</td>
                      <td><a href="{{ route('admin.menu.sort_form', $idx) }}">Edit Sorting</a></td>
                      </td>
                    </tr>
                      @foreach ($menus[$idx] as $key => $val)
                        @php
                          $value = App\Menu::find($val['id']);
                        @endphp
                        <tr class="even pointer child {{ $parent_index }}">
                          <td class="a-center ">
                          </td>
                          <td class="parent" child='{{ $parent_index }}-{{ $loop->index }}'>{{ $value->title }}</td>
                          <td class=" "><a href="{{ $value->url }}">{{ $value->url }}</a></td>
                          <td class=" last"><a href="{{ route('admin.menu.edit', $value->id_menu) }}">Edit</a>&nbsp;/&nbsp;<a href="{{ route('admin.menu.destroy', $value->id_menu) }}" class="delete-menu">Delete</a>
                          </td>
                        </tr>
                        @php
                          $index = $loop->index;
                        @endphp
                        @foreach (@$val['children'] ?: array() as $key2 => $val2)
                          @php
                            $value2 = App\Menu::find($val2['id']);
                          @endphp
                          <tr class="even pointer child-child {{ $parent_index }}-{{ $index }}" >
                            <td class="a-center ">
                            </td>
                            <td class="parent" child='{{ $parent_index }}-{{ $index }}-{{ $loop->index }}'>{{ $value2->title }}</td>
                            <td class=" "><a href="{{ $value2->url }}">{{ $value2->url }}</a></td>
                            <td class=" last"><a href="{{ route('admin.menu.edit', $value2->id_menu) }}">Edit</a>&nbsp;/&nbsp;<a href="{{ route('admin.menu.destroy', $value2->id_menu) }}" class="delete-menu">Delete</a>
                            </td>
                          </tr>
                          @php
                            $index2 = $loop->index;
                          @endphp
                          @foreach (@$val2['children'] ?: array() as $key3 => $val3)
                            @php
                              $value3 = App\Menu::find($val3['id']);
                            @endphp
                            <tr class="even pointer child-child-child {{ $parent_index }}-{{ $index }}-{{ $index2 }}" >
                              <td class="a-center ">
                              </td>
                              <td class="parent">{{ $value3->title }}</td>
                              <td class=" "><a href="{{ $value3->url }}">{{ $value3->url }}</a></td>
                              <td class=" last"><a href="{{ route('admin.menu.edit', $value3->id_menu) }}">Edit</a>&nbsp;/&nbsp;<a href="{{ route('admin.menu.destroy', $value3->id_menu) }}" class="delete-menu">Delete</a>
                              </td>
                            </tr>
                          @endforeach
                        @endforeach
                      @endforeach
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="modal fade modal-delete-menu" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
          </button>
          <h4 class="modal-title">Delete Menu</h4>
        </div>
        <div class="modal-body">
          <p>Apakah kamu yakin ingin menghapus menu ini?</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
          <button type="button" class="btn btn-danger" id="delete-menu-modal-button" data-url=""><span class="fa fa-trash"></span> Delete Menu</button>
        </div>
      </div>
    </div>
  </div>
  <form id="delete-menu-form" action="" method="post">
    <input type="hidden" name="_method" value="DELETE">
  </form>
  <script type="text/javascript">
    var DELETE_MENU_SUCCESS_LINK = "{{ route('admin.menu.index') }}";
  </script>
@endsection
