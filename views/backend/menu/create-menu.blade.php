@extends('backend.includes.header')

@section('content')
  <div class="right_col" role="main">
    <div class="">
      <div class="row">
        <div class="col-md-12">
          <div class="x_panel">
            <div class="x_title">
              <h3>Create new menu</h3>
              <div class="clearfix">
              </div>
            </div>
            <div class="x_content">
              <form id="create_menu_form" action="{{ route('admin.menu.store') }}" method="post" data-form-type="json">
                <div class="table-responsive">
                  <div class="col-md-9">
                    <div class="col-md-6">
                      <h4>Title</h4>
                      <input type="text" name="title" class="form-control" placeholder="Title">
                    </div>
                    <div class="col-md-6">
                      <h4>Link</h4>
                      <select class="form-control" name="menu_type">
                        <option value="genre">Genre</option>
                        <option value="tag">Tag</option>
                        <option value="page">Page</option>
                        <option value="keyword">Keyword</option>
                        <option value="external">External Link</option>
                      </select>
                    </div>
                    {{ csrf_field() }}
                    <div class="col-md-6">
                      <h4>Position</h4>
                      <select name="position" id="menu_position" class="select2_single form-control" tabindex="-1">
                        <option value="top">Top Menu</option>
                        <option value="bottom">Bottom Menu</option>
                        <option value="footer">Footer Menu</option>
                      </select>
                    </div>
                    <div class="col-md-6">
                      <h4>Parent Menu</h4>
                      <select name="parent_id" id="parent_menu" class="select2_single form-control" tabindex="-1">

                      </select>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12" style="padding-top:25px">
                      <h2>Description</h2>
                      <div class="x_content">
                        <div id="alerts"></div>
                        <textarea name="description" class="content" id="content"></textarea>
                      </div>
                    </div>

                  </div>
                </div>
                <div class="col-md-12" style="margin-top: 15px;">
                  <div class="pull-right">
                    <button class="btn btn-primary" type="reset">Reset</button>
                    <button type="submit" class="btn btn-success">Submit</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <script type="text/javascript">
    var CREATE_MENU_SUCCESS_LINK = "{{ route('admin.menu.index') }}";
    var PARENT_MENU_JSON_LINK = "{{ route('admin.menu.parent.json') }}";
    var CHILD_MENU_JSON_LINK = "{{ route('admin.menu.child.json') }}";
    var MENU_TYPE_LINK = "{{ route('admin.menu.type') }}";
  </script>
@endsection
