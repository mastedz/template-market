@extends('backend.includes.header')

@section('content')
  <div class="right_col" role="main">
    <div class="">
      <div class="row">
        <div class="col-md-12">
          <div class="x_panel">
            <div class="x_title">
              <h3>Edit menu</h3>
              <div class="clearfix">
              </div>
            </div>
            <div class="x_content">
              <form id="create_menu_form" action="{{ route('admin.menu.update', $menu->id_menu) }}" method="post" data-form-type="json">
                <input type="hidden" name="_method" value="PATCH">
                <div class="table-responsive">
                  <div class="col-md-9">
                    <div class="col-md-6">
                      <h4>Title</h4>
                      <input type="text" name="title" value="{{ $menu->title }}" class="form-control" placeholder="Title">
                    </div>
                    <div class="col-md-6">
                      <h4>Link</h4>
                      <select class="form-control" name="menu_type">
                        <option value="genre" {{ $menu->type == "genre" ? "selected" : "" }}>Genre</option>
                        <option value="tag" {{ $menu->type == "tag" ? "selected" : "" }}>Tag</option>
                        <option value="page" {{ $menu->type == "page" ? "selected" : "" }}>Page</option>
                        <option value="keyword" {{ $menu->type == "keyword" ? "selected" : "" }}>Keyword</option>
                        <option value="external" {{ $menu->type == "external" ? "selected" : "" }}>External Link</option>
                      </select>
                      <input type="hidden" name="current_type" value="{{ $menu->type }}">
                      <input type="hidden" name="current_val" value="{{ $menu->val }}">
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12" style="padding-top:25px">
                      <h2>Description</h2>
                      <div class="x_content">
                        <div id="alerts"></div>
                        <textarea name="description" class="content" id="content">{{ $menu->description }}</textarea>
                      </div>
                    </div>

                  </div>
                </div>
                <div class="col-md-12" style="margin-top: 15px;">
                  <div class="pull-right">
                    <button class="btn btn-primary" type="reset">Reset</button>
                    <button type="submit" class="btn btn-success">Submit</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <script type="text/javascript">
    var CREATE_MENU_SUCCESS_LINK = "{{ route('admin.menu.index') }}";
    var PARENT_MENU_JSON_LINK = "{{ route('admin.menu.parent.json') }}";
    var CHILD_MENU_JSON_LINK = "{{ route('admin.menu.child.json') }}";
    var MENU_TYPE_LINK = "{{ route('admin.menu.type') }}";
  </script>
@endsection
