@extends('backend.includes.header')

@section('content')
  <div class="right_col" role="main">
   
    <div class="">
      <div class="row">
        <div class="col-md-12">
          <div class="x_panel">
            <div class="x_title">
              <h3>Deatail Merchant</h3>
              <div class="clearfix">
              </div>
            </div>
            <form id="create_category_form" action="{{ route('admin.merchant.update', $merchant->id_merchant) }}" method="post">
              <input type="hidden" name="_method" value="PATCH">
              <div class="x_content">
               

                    <div class="row">

                      <h4>Dokumen Lampiran</h4>

                      <div class="col-md-55">
                        <div class="thumbnail">
                          <div class="image view view-first">
                            <img style="width: 100%; display: block;" src="{{asset('storage/images/' .$files['ktp']) }}" alt="image">
                            <div class="mask">
                              <p>Your Text</p>
                              <div class="tools tools-bottom">
                                <a href="#"><i class="fa fa-link"></i></a>
                                <a href="#"><i class="fa fa-pencil"></i></a>
                                <a href="#"><i class="fa fa-times"></i></a>
                              </div>
                            </div>
                          </div>
                          <div class="caption">
                            <p>KTP</p>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-55">
                        <div class="thumbnail">
                          <div class="image view view-first">
                            <img style="width: 100%; display: block;" src="{{asset('storage/images/' .$files['npwp']) }}" alt="image">
                            <div class="mask">
                              <p>Your Text</p>
                              <div class="tools tools-bottom">
                                <a href="#"><i class="fa fa-link"></i></a>
                                <a href="#"><i class="fa fa-pencil"></i></a>
                                <a href="#"><i class="fa fa-times"></i></a>
                              </div>
                            </div>
                          </div>
                          <div class="caption">
                            <p>NPWP</p>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-55">
                        <div class="thumbnail">
                          <div class="image view view-first">
                            <img style="width: 100%; display: block;" src="{{asset('storage/images/' .$files['logo']) }}" alt="image">
                            <div class="mask">
                              <p>Your Text</p>
                              <div class="tools tools-bottom">
                                <a href="#"><i class="fa fa-link"></i></a>
                                <a href="#"><i class="fa fa-pencil"></i></a>
                                <a href="#"><i class="fa fa-times"></i></a>
                              </div>
                            </div>
                          </div>
                          <div class="caption">
                            <p>LOGO</p>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-55">
                        <div class="thumbnail">
                          <div class="image view view-first">
                            <img style="width: 100%; display: block;" src="{{asset('storage/images/' .$files['product']) }}" alt="image">
                            <div class="mask">
                              <p>Your Text</p>
                              <div class="tools tools-bottom">
                                <a href="#"><i class="fa fa-link"></i></a>
                                <a href="#"><i class="fa fa-pencil"></i></a>
                                <a href="#"><i class="fa fa-times"></i></a>
                              </div>
                            </div>
                          </div>
                          <div class="caption">
                            <p>Contoh Product</p>
                          </div>
                        </div>
                      </div>
                      </div>
                 
                <div class="table-responsive">
                  <div class="col-md-9">
                    <div class="col-md-6">
                      <h4>Nama Merchant:</h4>
                      <h4>{{$merchant->shop()->first()->name}}
                    </div>
                     <div class="col-md-6">
                      <h4>Alamat Merchant:</h4>
                      <h4>{{$merchant->shop()->first()->shop_address}}
                    </div>
                     <div class="col-md-6">
                      <h4>Pemilik Merchant:</h4>
                      <h4>{{$merchant->user()->first()->name}}
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12" style="padding-top:25px">
                      <h2>Deskripsi Merchant:</h2>
                      <p>{{$merchant->shop()->first()->description}}</p>
                    </div>

                  </div>
                 

              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
 
@endsection
