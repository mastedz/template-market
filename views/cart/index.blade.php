@extends('frontend.layout_page')

@section('someCSS')
  <link rel="stylesheet" type="text/css" href="{{asset('styles/cart_styles.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('styles/cart_responsive.css')}}">
@endsection

@section('content')

 <div class="cart_section">
        <div class="container">
            <div class="row">
                <div class="col-lg-10 offset-lg-1">
                    <div class="cart_container">
                        
                        <div class="cart_title">Shopping Cart</div>
                         <form method="POST" action="/checkout" id="proceedCheckout">
                        @forelse($cartItems as $cartItem) 
                        <div class="cart_items">
                            
                            <ul class="cart_list">
                                <li class="cart_item clearfix">
                                    <div class="cart_item_image"><img src="images/shopping_cart.jpg" alt=""></div>
                                    <div class="cart_item_info d-flex flex-md-row flex-column justify-content-between">
                                        <div class="cart_item_name cart_info_col">
                                            <div class="cart_item_title">Pilih Produk</div>
                                           
                                            <div class="cart_item_text"><input type="checkbox" name="cart[]" value="{{$cartItem->rowId}}" class="form-control"> </div>
                                              {{ csrf_field() }}
                                        
                                        </div>
                                      
                                        <div class="cart_item_name cart_info_col">
                                            <div class="cart_item_title">Name</div>
                                            <div class="cart_item_text">{{$cartItem->name}}</div>
                                        </div>
                                        <div class="cart_item_quantity cart_info_col">
                                            <div class="cart_item_title">Quantity</div>
                                            <div class="cart_item_text">{{$cartItem->qty}}</div>
                                        </div>
                                        <div class="cart_item_price cart_info_col">
                                            <div class="cart_item_title">Price</div>
                                            <div class="cart_item_text">Rp {{$cartItem->price}}</div>
                                        </div>
                                        <div class="cart_item_total cart_info_col">
                                            <div class="cart_item_title">Total</div>
                                            <div class="cart_item_text">Rp. {{($cartItem->qty)*($cartItem->price)}}</div>
                                        </div>
                                        
                                        <div class="cart_item_total cart_info_col">
                                            <div class="cart_item_title">Action</div>
                                          
                                            <div class="cart_item_text"><a class="btn btn-warning" href="{{url('/delete/'.$cartItem->rowId)}}"><i class="fa fa-trash"></i></a> </div>
                                          
                                        </div>
                                    </div>
                                </li>
                            </ul>
                            
                        </div>
                         @empty 
                             @endforelse
                        </form>
                        <!-- Order Total 
                        <div class="order_total">
                            <div class="order_total_content text-md-right">
                                <div class="order_total_title">Order Total:</div>
                                <div class="order_total_amount">$2000</div>
                            </div>
                        </div>-->

                        <div class="cart_buttons">
                           
                            <button id="proceed" type="submit" class="button cart_button_checkout">Lanjutkan Ke Pembayaran</button>
                        </div>
                 
                    </div>
                </div>
            </div>
        </div>
    </div>
 

@endsection
@section('someJS')
<script type="text/javascript">
    $('#proceed').on('click', function(){
        $('#proceedCheckout').submit();
    })
</script>
@endsection